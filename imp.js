var OrientDB = require("orientjs");
var Promise = require('bluebird');

var config = require("./import-config.json");

var server = OrientDB({
   host:       config.host,
   port:       config.port,
   username:   config.server_username,
   password:   config.server_password
});


server.exists(config.database)
.then(function(exists) {
    if (!exists) {
        return server.create({name:config.database,storage:"plocal"});
    }
})
.then(function() {
    return server.use({ name:config.database, username:config.username, password:config.passowrd});
}).then(function(db) {
    Promise.all([
        db.query("create class Node if not exists extends V"),
        db.query("create class Link if not exists extends E"),
    ]).then(function() {

    var data = require(config.source_file);

    var vPromises = [];
    data.nodes.forEach(function (el) {
      vPromises.push(db.create('vertex','Node').set(el).one());
        });

    Promise.all(vPromises).then(function(vertices){
      var ePromises = [];
      data.links.forEach(function (el) {
        ePromises.push(
          db.create('edge','Link')
          .from(`select from Node where id = '${el.source}'`)
          .to(`select from Node where id = '${el.target}'`)
          .set(el)
          .one());
          });
      return Promise.all(ePromises);
    }).then(function(edges){
        return db.close();
    }).then((function(){
      process.exit();
    })).catch(function(err){
      console.log(err);
      process.exit();
    });

    });
});
